@extends('common.layout')
@section('title', 'Projects')
@section('product_title')
    @push('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.css" />
        <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.min.css') }}">
    @endpush
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Projects</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Projects</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="card-title">Projects</h3>
                                    </div>
                                    <div class="col-md-6 text-md-right">
                                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addModal"
                                            type="button"><i class="fa fa-plus"></i> Add</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="dt" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Client</th>
                                            <th>Members</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Add Brand Modal Start --}}

                <div class="modal" id="addModal" style="overflow-y: auto!important;">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Add Project</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            </div>
                            <div id="errorDiv"></div>

                            <form action="{{ route('project.store') }}" id="addForm" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name" class="control-label">Name <code>*</code></label>
                                                <input type="text" class="form-control" name="name"
                                                    placeholder="Please enter name of project" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="slug" class="control-label">Slug <code>*</code></label>
                                                <input type="text" class="form-control" name="slug" readonly required>
                                            </div>
                                            <div class="form-group">
                                                <label for="description" class="control-label">Description
                                                    <code>*</code></label>
                                                <textarea class="form-control summernote" name="description" placeholder="Please enter description of project" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="start_date" class="control-label">Start Date </label>
                                                <input type="date" class="form-control" name="start_date">
                                            </div>
                                            <div class="form-group">
                                                <label for="end_date" class="control-label">End Date </label>
                                                <input type="date" class="form-control" name="end_date">
                                            </div>
                                            <div class="form-group">
                                                <label for="client">Client </label>
                                                <select class="form-control" name="client">
                                                    <option value="">Select Client</option>
                                                    @foreach ($clients as $client)
                                                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="members">Members</label>
                                                <select id="member" class="form-control mb-3" multiple name="members[]">
                                                    <option>Select Members</option>
                                                    @foreach ($members as $member)
                                                        <option value="{{ $member->id }}">{{ $member->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                {{-- Add Brand Modal End --}}

                {{-- Edit Brand Modal Start --}}

                <div class="modal" id="editModal" style="overflow-y: auto!important;">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Edit Project</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            </div>
                            <form action="" id="editForm" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="errorDivEdit"></div>
                                            <input type="hidden" name="id">
                                            <div class="form-group">
                                                <label for="name" class="control-label">Name <code>*</code></label>
                                                <input type="text" class="form-control" name="name">
                                            </div>
                                            <div class="form-group" id="edit_slug">
                                                <label for="slug" class="control-label">Slug <code>*</code></label>
                                                <input type="text" class="form-control" name="slug" readonly
                                                    required>
                                            </div>
                                            <div class="form-group">
                                                <label for="description" class="control-label">Description
                                                    <code>*</code></label>
                                                <textarea class="form-control summernote" name="description"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="start_date" class="control-label">Start Date</label>
                                                <input type="date" class="form-control" name="start_date">
                                            </div>
                                            <div class="form-group">
                                                <label for="end_date" class="control-label">End Date</label>
                                                <input type="date" class="form-control" name="end_date">
                                            </div>
                                            <div class="form-group">
                                                <label for="client">Client</label>
                                                <select class="form-control" name="client">
                                                    <option>Select Client</option>
                                                    @foreach ($clients as $client)
                                                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="members">Members</label>
                                                <select id="member" class="form-control mb-3" multiple
                                                    name="members[]">
                                                    <option>Select Members</option>
                                                    @foreach ($members as $member)
                                                        <option value="{{ $member->id }}">{{ $member->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit">Update</button>
                                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {{-- Edit Brand Modal End --}}
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script>
        $('.summernote').summernote({
            height: 400,
            stripTags: ['style'],
        });
    </script>

    <script>
        $('.modal').on('shown.bs.modal', function(e) {
            $(e.target).find('select').select2();
        });
    </script>

    <script>
        $("#single, #member").select2({
            placeholder: "Please Select",
            color: "black",
            allowClear: true
        });
    </script>

    <script>
        window.DT = $('#dt').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            scrollX: true,
            ajax: location.href,
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'start_date',
                    name: 'start_date',
                    width: '25%'
                },
                {
                    data: 'end_date',
                    name: 'end_date',
                    width: '25%'
                },
                {
                    data: 'client',
                    name: 'client'
                },
                {
                    data: 'members',
                    name: 'members'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        });
    </script>
    <script>
        var $validator = $("#addForm").validate({
            rules: {
                name: {
                    required: true,
                },
                slug: {
                    required: true,
                },
                description: {
                    required: true,
                },

            }
        });
    </script>
    <script>
        $('#addForm').ajaxForm({
            beforeSubmit: function() {
                $.LoadingOverlay("show");
            },
            statusCode: {
                422: function(errors) {
                    var html = '';
                    $.each(errors.responseJSON.errors, function(key, value) {
                        html = html +
                            '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                            value + '</div>';
                    });
                    $('#errorDiv').html(html);
                    $('#errorDiv').fadeIn();
                    $.LoadingOverlay("hide");
                },
                500: function(error) {
                    toastr['error'](error.responseJSON.message);
                    $.LoadingOverlay("hide");
                },
                200: function(res) {
                    toastr[res.type](res.text);
                    $('#addForm')[0].reset();
                    $('#addModal').modal('toggle');
                    DT.ajax.reload();
                    $.LoadingOverlay("hide");
                }
            }
        });

        $('#editForm').ajaxForm({
            beforeSubmit: function() {
                $.LoadingOverlay("show");
            },
            statusCode: {
                422: function(errors) {
                    var html = '';
                    $.each(errors.responseJSON.errors, function(key, value) {
                        html = html +
                            '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                            value + '</div>';
                    });
                    $('#errorDivEdit').html(html);
                    $('#errorDivEdit').fadeIn();
                    $.LoadingOverlay("hide");
                },
                500: function(error) {
                    toastr['error'](error.responseJSON.message);
                    $.LoadingOverlay("hide");
                },

                200: function(res) {
                    toastr[res.type](res.text);
                    $('#editForm')[0].reset();
                    $('#editModal').modal('toggle');
                    DT.ajax.reload();
                    $.LoadingOverlay("hide");
                }
            }
        });

        function edit(id) {
            $('#editForm').attr('action', location.href + '/update/' + id);
            $.LoadingOverlay("show");
            $.ajax({
                url: location.href + '/edit/' + id,
                type: 'get',
                dataType: 'json',
                success: function(arg) {
                    $.each(arg, function(i, row) {
                        var elem = $('#editForm [name="' + i + '"]');
                        elem.val(row);
                    });
                    $('#editForm [name="client"]').val(arg.client_id).trigger('change');
                    $('#editForm [name="members[]"]').val(arg.members_arr).trigger('change');
                    $('#editForm [name="description"]').summernote('code', arg.description);
                }
            });
            $.LoadingOverlay("hide");
            $('#editModal').modal('show');
        }



        function del(id) {
            if (confirm("Are You Sure You Want To Delete This Project?")) {
                $.LoadingOverlay("show");
                $.ajax({
                    url: location.href + '/delete/' + id,
                    type: "DELETE",
                    dataType: "JSON",
                    data: {
                        _token: '{{ csrf_token() }}'
                    },
                    statusCode: {
                        422: function(errors) {
                            var html = '';
                            $.each(errors.responseJSON.errors, function(key, value) {
                                html = html +
                                    '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                                    value + '</div>';
                            });
                            $('#errorDiv').html(html);
                            $('#errorDiv').fadeIn();
                            $.LoadingOverlay("hide");
                        },
                        500: function(error) {
                            toastr['error'](error.responseJSON.message);
                            $.LoadingOverlay("hide");
                        },
                        200: function(res) {
                            toastr[res.type](res.text);
                            $.LoadingOverlay("hide");
                            DT.ajax.reload();
                        }
                    }
                });
            }
        }

        function status_change(id) {
            if (confirm("Are You Sure to Change Status of this Project?")) {
                $.LoadingOverlay("show");
                $.ajax({
                    url: "{{ route('project.status_change') }}",
                    type: "POST",
                    data: {
                        id: id,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: "JSON",
                    statusCode: {
                        422: function(errors) {
                            var html = '';
                            $.each(errors.responseJSON.errors, function(key, value) {
                                html = html +
                                    '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                                    value + '</div>';
                            });
                            $('#errorDiv').html(html);
                            $('#errorDiv').fadeIn();
                            $.LoadingOverlay("hide");
                        },
                        500: function(error) {
                            toastr['error'](error.responseJSON.message);
                            $.LoadingOverlay("hide");
                        },
                        200: function(res) {
                            toastr[res.type](res.text);
                            $.LoadingOverlay("hide");
                            DT.ajax.reload();
                        }
                    }
                });
            }
        }

        $('#addForm [name="name"]').keyup(function() {
            var Text = $(this).val();
            Text = Text.toLowerCase();
            var regExp = /\s+/g;
            Text = Text.replace(regExp, '-');
            $('#addForm [name="slug"]').val(Text);
        });

        $('#editForm [name="name"]').keyup(function() {
            var Text = $(this).val();
            Text = Text.toLowerCase();
            var regExp = /\s+/g;
            Text = Text.replace(regExp, '-');
            $('#editForm [name="slug"]').val(Text);
        });
    </script>
@endpush
