@extends('common.layout')
@section('title', 'Projects')
@section('product_title')

    @push('styles')
        <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.min.css') }}">

        <style>
            #password-strength-status {
                padding: 10px 10px;
                border-radius: 4px;
                margin-top: 5px;
                margin-bottom: 5px;

            }

            #password-strength-status-edit {
                padding: 10px 10px;
                border-radius: 4px;
                margin-top: 5px;
                margin-bottom: 5px;

            }

            .medium-password {
                background-color: #fd0;
            }

            .weak-password {
                background-color: #FBE1E1;
            }

            .strong-password {
                background-color: #D5F9D5;
            }
        </style>
    @endpush
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Supervisors</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Supervisors</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="card-title">Supervisors</h3>
                                    </div>
                                    <div class="col-md-6 text-md-right">
                                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addModal"
                                            type="button"><i class="fa fa-plus"></i> Add</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="dt" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Address</th>
                                            <th>Date of Joining</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Add Brand Modal Start --}}

                <div class="modal" id="addModal" style="overflow-y: auto!important;">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Add Supervisor</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            </div>

                            <form action="{{ route('supervisor.store') }}" id="addForm" method="POST"
                                enctype="multipart/form-data" autocomplete="nope">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="errorDiv"></div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label for="name" class="control-label">Name <code>*</code></label>
                                                    <input type="text" class="form-control" name="name"
                                                        placeholder="Please Enter Name of Supervisor" required>
                                                </div>
                                                <div class="col-6">
                                                    <label for="email" class="control-label">Email <code>*</code></label>
                                                    <input type="email" class="form-control" name="email"
                                                        placeholder="Please Enter Email of Supervisor" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label for="mobile" class="control-label">Mobile
                                                        <code>*</code></label>
                                                    <input type="text" class="form-control" name="mobile"
                                                        placeholder="Please Enter Mobile No. of Supervisor" required>
                                                </div>
                                                <div class="col-6">
                                                    <label for="start_date" class="control-label">Date Of Joining</label>
                                                    <input type="date" class="form-control" name="date_of_joining">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="address" class="control-label">Address</label>
                                                    <textarea class="form-control" name="address" placeholder="Please enter Supervisor's Address"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label for="password" class="control-label">Password
                                                        <code>*</code></label>
                                                    <input type="password" class="form-control" name="password"
                                                        placeholder="Please Enter Password" onkeyup="checkStrength();"
                                                        required autocomplete="false">
                                                </div>
                                                <div class="col-6">
                                                    <label for="cnf_password" class="control-label">Confirm
                                                        Password<code>*</code></label>
                                                    <input type="password" class="form-control" name="cnf_password"
                                                        placeholder="Please Confirm Password" required>
                                                </div>
                                            </div>
                                            <div id="password-strength-status" class="text-center">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                {{-- Add Brand Modal End --}}

                {{-- Edit Brand Modal Start --}}

                <div class="modal" id="editModal" style="overflow-y: auto!important;">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Edit Supervisor</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            </div>
                            <form method="post" enctype="multipart/form-data" id="editForm" autocomplete="off">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="errorDivEdit"></div>
                                            <input type="hidden" name="id">
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label for="name" class="control-label">Name
                                                        <code>*</code></label>
                                                    <input type="text" class="form-control" name="name"
                                                        placeholder="Please Enter Name of Supervisor" required>
                                                </div>

                                                <div class="col-6">
                                                    <label for="email" class="control-label">Email
                                                        <code>*</code></label>
                                                    <input type="email" class="form-control" name="email"
                                                        placeholder="Please Enter Email of Supervisor" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label for="mobile" class="control-label">Mobile
                                                        <code>*</code></label>
                                                    <input type="text" class="form-control" name="mobile"
                                                        placeholder="Please Enter Mobile No. of Supervisor" required>
                                                </div>
                                                <div class="col-6">
                                                    <label for="start_date" class="control-label">Date Of Joining</label>
                                                    <input type="date" class="form-control" name="date_of_joining">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="address" class="control-label">Address</label>
                                                    <textarea class="form-control" name="address" placeholder="Please enter Supervisor's Address"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label for="password" class="control-label">New Password</label>
                                                    <input type="password" class="form-control" name="password"
                                                        placeholder="Please Enter Password" onkeyup="checkStrengthEdit();"
                                                        autocomplete="false">
                                                </div>
                                                <div class="col-6">
                                                    <label for="cnf_password" class="control-label">Confirm
                                                        Password</label>
                                                    <input type="password" class="form-control" name="cnf_password"
                                                        placeholder="Please Confirm Your Password">
                                                </div>
                                            </div>
                                            <div id="password-strength-status-edit" class="text-center">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit">Update</button>
                                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {{-- Edit Brand Modal End --}}
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function() {
            $('input').attr('autocomplete', 'off');
        });
        $('.summernote').summernote({
            height: 150,
            stripTags: ['style'],
        });
    </script>
    <script>
        window.DT = $('#dt').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            scrollX: true,
            ajax: location.href,
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name',
                    width: '20%'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'mobile',
                    name: 'mobile'
                },
                {
                    data: 'address',
                    name: 'address',
                    width: '20%'
                },
                {
                    data: 'date_of_joining',
                    name: 'date_of_joining'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        });
    </script>
    <script>
        function checkStrength() {
            var number = /([0-9])/;
            var lower_case = /([a-z])/;
            var upper_case = /([A-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#addForm input[name="password"]').val().length < 8) {
                $('#addForm #password-strength-status').removeClass();
                $('#addForm #password-strength-status').addClass('weak-password');
                $('#addForm #password-strength-status').html("Weak (should be atleast 8 characters.)");
                return false;
            } else {
                if ($('#addForm input[name="password"]').val().match(number) && $('#addForm input[name="password"]').val()
                    .match(lower_case) && $('#addForm input[name="password"]').val().match(upper_case) && $(
                        '#addForm input[name="password"]').val().match(special_characters)) {
                    $('#addForm #password-strength-status').removeClass();
                    $('#addForm #password-strength-status').addClass('strong-password');
                    $('#addForm #password-strength-status').html("Strong");
                    return true;
                } else {
                    $('#addForm #password-strength-status').removeClass();
                    $('#addForm #password-strength-status').addClass('medium-password');
                    $('#addForm #password-strength-status').html(
                        "Medium (should include one upper case and one lower case alphabet, numbers and special characters.)"
                    );
                    return false;
                }
            }
        }

        function checkStrengthEdit() {
            var number = /([0-9])/;
            var lower_case = /([a-z])/;
            var upper_case = /([A-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#editForm input[name="password"]').val().length < 8) {
                $('#editForm #password-strength-status-edit').removeClass();
                $('#editForm #password-strength-status-edit').addClass('weak-password');
                $('#editForm #password-strength-status-edit').html("Weak (should be atleast 8 characters.)");
                return false;
            } else {
                if ($('#editForm input[name="password"]').val().match(number) && $('#editForm input[name="password"]').val()
                    .match(lower_case) && $('#editForm input[name="password"]').val().match(upper_case) && $(
                        '#editForm input[name="password"]').val().match(special_characters)) {
                    $('#editForm #password-strength-status-edit').removeClass();
                    $('#editForm #password-strength-status-edit').addClass('strong-password');
                    $('#editForm #password-strength-status-edit').html("Strong");
                    return true;
                } else {
                    $('#editForm #password-strength-status-edit').removeClass();
                    $('#editForm #password-strength-status-edit').addClass('medium-password');
                    $('#editForm #password-strength-status-edit').html(
                        "Medium (should include one upper case and one lower case alphabet, numbers and special characters.)"
                    );
                    return false;
                }
            }
        }
        var $validator = $("#addForm").validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                },
                mobile: {
                    required: true,
                },
                password: {
                    required: true,
                },
                cnf_password: {
                    required: true,
                }
            }
        });
    </script>
    <script>
        $('#addForm').ajaxForm({
            dataType: 'json',
            beforeSubmit: function(arr, $form, options) {
                $('button[type="submit"]').LoadingOverlay("show");
                if (checkStrength() == false) {
                    toastr['error']('Error in new password field, New password should be strong.');
                    $('button[type="submit"]').LoadingOverlay("hide");
                    return false;
                }

                $('#addForm .errorDiv').hide();
                $('#addForm').LoadingOverlay("show");
                return true;
            },
            success: function(res) {
                $('#addForm').LoadingOverlay("hide");
                $('button[type="submit"]').LoadingOverlay("hide");
                toastr[res.type](res.text);
                window.setTimeout(function() {
                    location.reload();
                }, 2000);
            },
            error: function(data) {
                $('#addForm').LoadingOverlay("hide");
                $('button[type="submit"]').LoadingOverlay("hide");
                toastr['error'](data.responseJSON.message);
            }
        });

        $('#editForm').ajaxForm({
            beforeSubmit: function() {
                $('button[type="submit"]').LoadingOverlay("show");
                if ($('#editForm input[name="password"]').val().length > 0) {
                    if (checkStrengthEdit() == false) {
                        toastr['error']('Error in new password field,new password should be strong.');
                        $('button[type="submit"]').LoadingOverlay("hide");
                        return false;
                    }
                }
                $('#editForm .errorDiv').hide();
                $('#editForm').LoadingOverlay("show");
                return true;
            },
            success: function(res) {
                $('#editForm').LoadingOverlay("hide");
                $('button[type="submit"]').LoadingOverlay("hide");
                toastr[res.type](res.message);
                window.setTimeout(function() {
                    location.reload();
                }, 2000);
            },
            error: function(data) {
                $('#editForm').LoadingOverlay("hide");
                $('button[type="submit"]').LoadingOverlay("hide");
                toastr['error'](data.responseJSON.message);
            }
        });

        function edit(id) {
            $('#editForm').attr('action', location.href + '/' + id);
            $.LoadingOverlay("show");
            $.ajax({
                url: location.href + '/' + id + '/edit',
                type: 'get',
                dataType: 'json',
                success: function(arg) {
                    $.each(arg, function(i, row) {
                        var elem = $('#editForm [name="' + i + '"]');
                        elem.val(row);
                    });
                }
            });
            $.LoadingOverlay("hide");
            $('#editModal').modal('show');
        }



        function del(id) {
            if (confirm("Are You Sure You Want To Delete this Supervisor?")) {
                $.LoadingOverlay("show");
                $.ajax({
                    url: location.href + '/' + id,
                    type: "DELETE",
                    dataType: "JSON",
                    data: {
                        id: id,
                        _token: '{{ csrf_token() }}'
                    },
                    statusCode: {
                        422: function(errors) {
                            var html = '';
                            $.each(errors.responseJSON.errors, function(key, value) {
                                html = html +
                                    '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                                    value + '</div>';
                            });
                            $('#errorDiv').html(html);
                            $('#errorDiv').fadeIn();
                            $.LoadingOverlay("hide");
                        },
                        500: function(error) {
                            toastr['error'](error.responseJSON.message);
                            $.LoadingOverlay("hide");
                        },
                        200: function(res) {
                            toastr[res.type](res.text);
                            $.LoadingOverlay("hide");
                            DT.ajax.reload();
                        }
                    }
                });
            }
        }

        function status_change(id) {
            if (confirm("Are You Sure to Change Status of this Supervisor?")) {
                $.LoadingOverlay("show");
                $.ajax({
                    url: "{{ route('supervisor.status_change') }}",
                    type: "POST",
                    data: {
                        id: id,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: "JSON",
                    statusCode: {
                        422: function(errors) {
                            var html = '';
                            $.each(errors.responseJSON.errors, function(key, value) {
                                html = html +
                                    '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                                    value + '</div>';
                            });
                            $('#errorDiv').html(html);
                            $('#errorDiv').fadeIn();
                            $.LoadingOverlay("hide");
                        },
                        500: function(error) {
                            toastr['error'](error.responseJSON.message);
                            $.LoadingOverlay("hide");
                        },
                        200: function(res) {
                            toastr[res.type](res.text);
                            $.LoadingOverlay("hide");
                            DT.ajax.reload();
                        }
                    }
                });
            }
        }
    </script>
@endpush
