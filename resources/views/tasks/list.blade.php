@extends('common.layout')
@section('title', 'Tasks')
@section('product_title')
    @push('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.css" />
        <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    @endpush
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Tasks</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Tasks</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="col-md-4">
                                    <label for="">Search by Date</label>
                                    <input type="text" class="form-control" name="datetimes" id="datetimes"
                                        style="width: 100%">
                                </div>
                                <div class="col-4" style="margin-top: 24px">
                                    <button type="button" class="btn btn-primary btn-sm" id="btnFilter">
                                        Search</i></button>
                                    <button type="button" class="btn btn-info btn-sm" id="reset">
                                        Reset</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="card-title">Tasks</h3>
                                    </div>
                                    <div class="col-md-6 text-md-right">
                                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addModal"
                                            type="button"><i class="fa fa-plus"></i> Add</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="dt" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Task Name</th>
                                            <th>Project Name</th>
                                            <th>Member Name</th>
                                            <th>Description</th>
                                            <th>Start Date & Time</th>
                                            <th>End Date & Time</th>
                                            <th>Submit Date & Time</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Add Brand Modal Start --}}

                <div class="modal" id="addModal" style="overflow-y: auto!important;">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Add Task</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            </div>
                            <div id="errorDiv"></div>
                            <form action="{{ route('task.store') }}" id="addForm" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="errorDiv"></div>
                                            <div class="form-group">
                                                <label for="name" class="control-label">Name of
                                                    Task<code>*</code></label>
                                                <input type="text" class="form-control" name="name"
                                                    placeholder="Please enter name of task" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="description" class="control-label">Description
                                                    <code>*</code></label>
                                                <textarea class="form-control summernote" name="description" placeholder="Please enter description of project" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="start_date_time" class="control-label">Start Date Time
                                                    <code>*</code></label>
                                                <input type="datetime-local" class="form-control" name="start_date_time"
                                                    required>
                                            </div>
                                            <div class="form-group">
                                                <label for="end_date_time" class="control-label">End Date Time
                                                </label>
                                                <input type="datetime-local" class="form-control" name="end_date_time">
                                            </div>
                                            <div class="form-group">
                                                <label for="project">Project </label>
                                                <select class="form-control" id="project" name="project">
                                                    <option value="">Select Project</option>
                                                    @foreach ($projects as $project)
                                                        <option value="{{ $project->id }}">{{ $project->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="member">Member </label>
                                                <select id="member-dd" class="form-control" multiple name="members[]">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                {{-- Add Brand Modal End --}}

                {{-- Edit Brand Modal Start --}}

                <div class="modal" id="editModal" style="overflow-y: auto!important;">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Edit Project</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            </div>
                            <form action="" id="editForm" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="errorDivEdit"></div>
                                            <input type="hidden" name="id">
                                            <div class="form-group">
                                                <label for="name" class="control-label">Name of
                                                    Task<code>*</code></label>
                                                <input type="text" class="form-control" name="name"
                                                    placeholder="Please enter name of task" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="description" class="control-label">Description
                                                    <code>*</code></label>
                                                <textarea class="form-control summernote" name="description" placeholder="Please enter description of project"
                                                    required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="start_date_time" class="control-label">Start Date
                                                    <code>*</code></label>
                                                <input type="datetime-local" class="form-control" name="start_date_time"
                                                    required>
                                            </div>
                                            <div class="form-group">
                                                <label for="end_date_time" class="control-label">End Date
                                                </label>
                                                <input type="datetime-local" class="form-control" name="end_date_time">
                                            </div>
                                            <div class="form-group">
                                                <label for="project">Project </label>
                                                <select class="form-control" id="project-edit" name="project">
                                                    <option value=""></option>
                                                    @foreach ($projects as $project)
                                                        <option value="{{ $project->id }}">{{ $project->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="member">Member </label>
                                                <select class="form-control" multiple name="members[]" id="member-edit">
                                                    @foreach ($members as $member)
                                                        <option value="{{ $member->id }}">{{ $member->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit">Update</button>
                                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                {{-- Edit Brand Modal End --}}

                {{-- Complete Brand Modal Start --}}

                <div class="modal" id="completeModal" style="overflow-y: auto!important;">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Submit Project</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            </div>
                            <form action="" id="completeForm" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="errorDivEdit"></div>
                                            <input type="hidden" name="id">
                                            <div class="form-group">
                                                <label for="delay_reason" class="control-label">Delay
                                                    Reason<code>*</code></label>
                                                <input type="text" class="form-control" name="delay_reason" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                {{-- Complete Brand Modal End --}}
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        var start_date = null;
        var end_date = null;

        $(function() {
            $('input[name="datetimes"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });
            $('input[name="datetimes"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format(
                    'DD/MM/YYYY'));
                start_date = picker.startDate.format('YYYY-MM-DD');
                end_date = picker.endDate.format('YYYY-MM-DD');
            });
            $('input[name="datetimes"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                start_date = '';
                end_date = '';
            });

        });
    </script>
    <script>
        $('.summernote').summernote({
            height: 400,
            stripTags: ['style'],
        });
    </script>

    <script>
        $('.modal').on('shown.bs.modal', function(e) {
            $(e.target).find('select').select2();
        });
    </script>

    <script>
        $("#single, #member").select2({
            placeholder: "Please Select",
            color: "black",
            allowClear: true
        });
        // $("#multiple").select2({
        //     placeholder: "Please Select",
        //     allowClear: true
        // });
    </script>

    <script>
        $(document).ready(function() {
            $('#project').on('change', function() {
                var idProject = this.value;
                $("#member-dd").html('');
                $.ajax({
                    url: "{{ url('fetch-members') }}",
                    type: "POST",
                    data: {
                        project_id: idProject,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $('#member-dd').html('');
                        $.each(result.members, function(key, value) {
                            $("#member-dd").append('<option value="' + value
                                .member_id + '">' + value.user.name + '</option>');
                        });
                    }
                });
            });

        });

        function fetchMembersAndSetPreselected(projectId, preselectedMembers) {
            $("#member-edit").html('');
            $.ajax({
                url: "{{ url('fetch-members') }}",
                type: "POST",
                data: {
                    project_id: projectId,
                    _token: '{{ csrf_token() }}'
                },
                dataType: 'json',
                success: function(result) {
                    $('#member-edit').html('');
                    $.each(result.members, function(key, value) {
                        $("#member-edit").append('<option value="' + value.member_id +
                            '">' + value.user.name + '</option>');
                    });

                    // Now set the preselected members
                    if (preselectedMembers) {
                        $('#member-edit').val(preselectedMembers).trigger('change');
                    }
                }
            });
        }

        $('#btnFilter').click(function() {
            $('#dt').DataTable().draw(true);
            $('input[name="csv_start_date"]').val(start_date);
        });
        $('#reset').click(function() {
            location.reload();
        });
    </script>

    <script>
        window.DT = $('#dt').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            scrollX: true,
            ajax: {
                url: location.href,
                data: function(d) {
                    d.start_date = start_date;
                    d.end_date = end_date;
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'project_name',
                    name: 'project_name'
                },
                {
                    data: 'member_name',
                    name: 'member_name'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'start_date_time',
                    name: 'start_date_time',
                    width: '30%'
                },
                {
                    data: 'end_date_time',
                    name: 'end_date_time',
                    width: '30%'
                },
                {
                    data: 'submit_datetime',
                    name: 'submit_datetime',
                    width: '30%'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        });
    </script>
    <script>
        $('#addForm').ajaxForm({
            beforeSubmit: function() {
                $.LoadingOverlay("show");
            },
            statusCode: {
                422: function(errors) {
                    var html = '';
                    $.each(errors.responseJSON.errors, function(key, value) {
                        html = html +
                            '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                            value + '</div>';
                    });
                    $('#errorDiv').html(html);
                    $('#errorDiv').fadeIn();
                    $.LoadingOverlay("hide");
                },
                500: function(error) {
                    toastr['error'](error.responseJSON.message);
                    $.LoadingOverlay("hide");
                },
                200: function(res) {
                    toastr[res.type](res.text);
                    $('#addForm')[0].reset();
                    $('#addModal').modal('toggle');
                    DT.ajax.reload();
                    $.LoadingOverlay("hide");
                }
            }
        });

        $('#editForm').ajaxForm({
            beforeSubmit: function() {
                $.LoadingOverlay("show");
            },
            statusCode: {
                422: function(errors) {
                    var html = '';
                    $.each(errors.responseJSON.errors, function(key, value) {
                        html = html +
                            '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                            value + '</div>';
                    });
                    $('#errorDivEdit').html(html);
                    $('#errorDivEdit').fadeIn();
                    $.LoadingOverlay("hide");
                },
                500: function(error) {
                    toastr['error'](error.responseJSON.message);
                    $.LoadingOverlay("hide");
                },

                200: function(res) {
                    toastr[res.type](res.text);
                    $('#editForm')[0].reset();
                    $('#editModal').modal('toggle');
                    DT.ajax.reload();
                    $.LoadingOverlay("hide");
                }
            }
        });

        function edit(id) {
            $('#editForm').attr('action', location.href + '/update/' + id);
            $.LoadingOverlay("show");
            $.ajax({
                url: location.href + '/edit/' + id,
                type: 'get',
                dataType: 'json',
                success: function(arg) {
                    $.each(arg, function(i, row) {
                        var elem = $('#editForm [name="' + i + '"]');
                        elem.val(row);
                    });
                    console.log(arg.user_id);
                    $('#editForm [name="project"]').val(arg.project_id).trigger('change');
                    fetchMembersAndSetPreselected(arg.project_id, arg.members_arr);
                    $('#editForm [name="description"]').summernote('code', arg.description);
                }
            });
            $.LoadingOverlay("hide");
            $('#editModal').modal('show');
        }

        function del(id) {
            if (confirm("Are You Sure You Want To Delete This Task?")) {
                $.LoadingOverlay("show");
                $.ajax({
                    url: location.href + '/delete/' + id,
                    type: "DELETE",
                    dataType: "JSON",
                    data: {
                        _token: '{{ csrf_token() }}'
                    },
                    statusCode: {
                        422: function(errors) {
                            var html = '';
                            $.each(errors.responseJSON.errors, function(key, value) {
                                html = html +
                                    '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                                    value + '</div>';
                            });
                            $('#errorDiv').html(html);
                            $('#errorDiv').fadeIn();
                            $.LoadingOverlay("hide");
                        },
                        500: function(error) {
                            toastr['error'](error.responseJSON.message);
                            $.LoadingOverlay("hide");
                        },
                        200: function(res) {
                            toastr[res.type](res.text);
                            $.LoadingOverlay("hide");
                            DT.ajax.reload();
                        }
                    }
                });
            }
        }

        function status_change(id) {
            if (confirm("Are You Sure to Change Status of this Task?")) {
                $.LoadingOverlay("show");
                $.ajax({
                    url: "{{ route('task.status_change') }}",
                    type: "POST",
                    data: {
                        id: id,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: "JSON",
                    statusCode: {
                        422: function(errors) {
                            var html = '';
                            $.each(errors.responseJSON.errors, function(key, value) {
                                html = html +
                                    '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                                    value + '</div>';
                            });
                            $('#errorDiv').html(html);
                            $('#errorDiv').fadeIn();
                            $.LoadingOverlay("hide");
                        },
                        500: function(error) {
                            toastr['error'](error.responseJSON.message);
                            $.LoadingOverlay("hide");
                        },
                        200: function(res) {
                            toastr[res.type](res.text);
                            $.LoadingOverlay("hide");
                            DT.ajax.reload();
                        }
                    }
                });
            }
        }

        function complete(id) {
            $('#completeForm').attr('action', location.href + '/complete_submit/' + id);
            $.LoadingOverlay("show");
            $.ajax({
                url: location.href + '/complete/' + id,
                type: 'get',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(arg) {
                    $.each(arg, function(i, row) {
                        var elem = $('#completeForm [name="' + i + '"]');
                        elem.val(row);
                    });

                    $('#completeForm [name="id"]').val(id);
                    var now = new Date();
                    var date_time = moment(now).format('YYYY-MM-DD HH:mm:ss');

                    if (arg.end_date_time != null) { // if  end_date_time exist
                        var new_date_time = moment(new Date(arg.end_date_time)).format('YYYY-MM-DD HH:mm:ss');

                        if (date_time <= new_date_time) { // for no delay
                            console.log('no delay');
                            complete_submit(id)
                        } else if (date_time >= new_date_time) { // for delay
                            console.log('delay');
                            $.LoadingOverlay("hide");
                            $('#completeModal').modal('show');
                        }
                    } else { // if no end date time
                        complete_submit(id)
                    }
                }
            });
        }

        $('#completeForm').ajaxForm({
            beforeSubmit: function() {
                $.LoadingOverlay("show");
            },
            statusCode: {
                422: function(errors) {
                    var html = '';
                    $.each(errors.responseJSON.errors, function(key, value) {
                        html = html +
                            '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                            value + '</div>';
                    });
                    $('#errorDiv').html(html);
                    $('#errorDiv').fadeIn();
                    $.LoadingOverlay("hide");
                },
                500: function(error) {
                    toastr['error'](error.responseJSON.message);
                    $.LoadingOverlay("hide");
                },
                200: function(res) {
                    toastr[res.type](res.text);
                    $('#completeForm')[0].reset();
                    $('#completeModal').modal('toggle');
                    DT.ajax.reload();
                    $.LoadingOverlay("hide");
                }
            }
        });


        function complete_submit(id) {
            $.ajax({
                url: location.href + '/complete_submit/' + id,
                type: "POST",
                data: {
                    id: id,
                    _token: '{{ csrf_token() }}'
                },
                dataType: "JSON",
                statusCode: {
                    422: function(errors) {
                        var html = '';
                        $.each(errors.responseJSON.errors, function(key, value) {
                            html = html +
                                '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div ></div>' +
                                value + '</div>';
                        });
                        $('#errorDiv').html(html);
                        $('#errorDiv').fadeIn();
                        $.LoadingOverlay("hide");
                    },
                    500: function(error) {
                        toastr['error'](error.responseJSON.message);
                        $.LoadingOverlay("hide");
                    },
                    200: function(res) {
                        toastr[res.type](res.text);
                        DT.ajax.reload();
                        $.LoadingOverlay("hide");
                    }
                }
            });
        }
    </script>
@endpush
