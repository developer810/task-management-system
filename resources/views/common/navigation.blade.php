<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('dashboard') }}" class="brand-link">
        <!-- <img src="dist/img/logo.PNG" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" > -->
        <span class="brand-text font-weight-light">
            {{-- <img src="{{ asset('assets/dist/img/logo.PNG') }}" alt="AdminLTE Logo" width="100%"> --}}
            <h4>TASK MANAGEMENT <br>SYSTEM</h4>
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        @php
            $user = auth()->user();
        @endphp
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('assets/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ ucwords(strtolower($user->name)) }}</a>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
                <li class="nav-item menu-open">
                    <a href="{{ route('dashboard') }}"
                        class="{{ request()->is('dashboard*') ? 'nav-link active' : 'nav-link' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                @if ($user->role_id == '1')
                    <li class="nav-item menu-open">
                        <a href="{{ route('supervisor.index') }}"
                            class="{{ request()->is('supervisor*') ? 'nav-link active' : 'nav-link' }}">
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                Supervisor
                            </p>
                        </a>
                    </li>
                @endif
                @if ($user->role_id == '1' || $user->role_id == '2')
                    <li class="nav-item menu-open">
                        <a href="{{ route('staff.index') }}"
                            class="{{ request()->is('staff*') ? 'nav-link active' : 'nav-link' }}">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Staff
                            </p>
                        </a>
                    </li>
                @endif
                @if ($user->role_id == '1' || $user->role_id == '2')
                    <li class="nav-item menu-open">
                        <a href="{{ route('client.index') }}"
                            class="{{ request()->is('client*') ? 'nav-link active' : 'nav-link' }}">
                            <i class="nav-icon fas fa-user-circle"></i>
                            <p>
                                Client
                            </p>
                        </a>
                    </li>
                @endif
                @if ($user->role_id == '1')
                    <li class="nav-item menu-open">
                        <a href="{{ route('project.index') }}"
                            class="{{ request()->is('project*') ? 'nav-link active' : 'nav-link' }}">
                            <i class="nav-icon fas fa-project-diagram"></i>
                            <p>
                                Project
                            </p>
                        </a>
                    </li>
                @endif
                @if ($user->role_id == '1' || $user->role_id == '2' || $user->role_id == '3')
                    <li class="nav-item menu-open">
                        <a href="{{ route('task.index') }}"
                            class="{{ request()->is('task*') ? 'nav-link active' : 'nav-link' }}">
                            <i class="nav-icon fas fa-tasks"></i>
                            <p>
                                Task
                            </p>
                        </a>
                    </li>
                @endif

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
