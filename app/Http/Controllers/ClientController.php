<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\Datatables\DataTables;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Datatables $datatables)
    {
        if ($datatables->getRequest()->ajax()) {

            $items = User::where('role_id',4)->orderBy('id', 'desc');

            global $index;

            $index = 1;

            return $datatables->of($items)

                ->addColumn('status', function ($items) {
                    if ($items->status == 1) {
                        $status = '<span class="badge badge-pill badge-success">Active</span>';
                    } else {
                        $status = '<span class="badge badge-pill badge-danger">Inactive</span>';
                    }
                    return $status;
                })
                ->addColumn('business_name', function ($items) {
                    if ($items->business_name) {
                        return $items->business_name;
                    } else {
                        return 'N/A';
                    }
                })

                ->addColumn('address', function ($items) {
                    if ($items->address) {
                        return $items->address;
                    } else {
                        return 'N/A';
                    }
                })

                ->addColumn('action', function ($items) {
                    $action = '<div class="btn-group btn-group-sm" role="group" aria-label="btnGroup1">';
                    if ($items->status == 0) {
                        $action .= '<button title="Active" type="button" class="btn btn-success btn-sm" onclick="status_change(\'' . $items->id . '\')">Active</button><button title="Edit" type="button" class="btn btn-primary btn-sm" onclick="edit(\'' . $items->id . '\')">Edit</button><button title="Delete" type="button" class="btn btn-danger btn-sm" onclick="del(\'' . $items->id . '\')">Delete</button></div>';
                    } else {
                        $action .= '<button title="Active" type="button" class="btn btn-warning btn-sm" onclick="status_change(\'' . $items->id . '\')">Inactive</button><button title="Edit" type="button" class="btn btn-primary btn-sm" onclick="edit(\'' . $items->id . '\')">Edit</button><button title="Delete" type="button" class="btn btn-danger btn-sm" onclick="del(\'' . $items->id . '\')">Delete</button></div>';
                    }
                    return $action;
                })
                ->addColumn('id', function ($items) {
                    global $index;
                    $id = $index;
                    $index++;
                    return $id;
                })
                ->rawColumns(['id', 'name', 'email', 'mobile', 'address', 'business_name', 'status', 'action'])
                ->make();
        }

        return view('client.list');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email:rfc,dns|max:255|unique:users,email',
            'mobile' => 'required|numeric|digits_between:10,10|unique:users,mobile',
            'address' => 'nullable|string|max:255',
            'password' => 'required|min:8',
            'cnf_password' => 'required_with:password|same:password|min:8',
            'business_name' => 'nullable|string|max:255'
        ]);

        if ($validator->fails()) {
    		return response()->json(['type'=>'error','message'=>$validator->messages()->all()],422);
        }

        $user = new User();
        $user->role_id = '4';
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile =$request->mobile;
        $user->password = bcrypt($request->password);
        $user->address = $request->address;
        $user->business_name = $request->business_name;

        if ($user->save()) {
            return response()->json(['type' => 'success', 'text' => 'Client added Successfully'], 200);
        }else{
            return response()->json(['type'=>'error','message'=>"Internal server error"],422);
        }
        return response()->json(['success' => false], 500);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user=User::where('id',$id)->first();
        return [
            'id' =>$user->id,
    		'name' => $user->name,
    		'email' => $user->email,
    		'mobile' => $user->mobile,
    		'address' => $user->address,
    		'business_name' => $user->business_name,
    	];
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $user=User::where('id',$request->id)->first();

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email:rfc,dns|max:255|unique:users,email,' . $user->id,
            'mobile' => 'required|numeric|digits_between:10,10|unique:users,mobile,' . $user->id,
            'address' => 'nullable|string|max:255',
            'password' => 'required_with:cnf_password',
            'cnf_password' => 'same:password',
            'business_name' => 'nullable|string|max:255'
        ]);

        if ($validator->fails()) {
    		return response()->json(['type'=>'error','message'=>$validator->messages()->all()],422);
        }

        if($user){
            $user->name = $request->name;
            $user->email = $request->email;
            $user->mobile =$request->mobile;
            $user->password = bcrypt($request->password);
            $user->address = $request->address;
            $user->business_name = $request->business_name;
            if($user->save()){
                return response()->json(['type'=>'success','message'=>'Client has been updated successfully'],200);
            }else{
                return response()->json(['type'=>'error','message'=>"Internal server error"],422);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $user = User::find($request->id);
        if ($user->delete()) {
            return response()->json(['type' => 'success', 'text' => 'Client Deleted Successfully'], 200);
        }
        return response()->json(['success' => false], 500);
    }

    function status_change(Request $request)
    {
        if ($request->id) {
            $user = User::find($request->id);
            if ($user->status == 0) {
               $user->status='1';
               if($user->save()){
                return response()->json(['type' => 'success', 'text' => 'Client Activated Successfully'], 200);
               }
            } else {
                $user->status='0';
                if($user->save()){
                    return response()->json(['type' => 'success', 'text' => 'Client Deactivated Successfully'], 200);
                }
            }
        }
        return response()->json(['success' => false], 500);
    }
}
