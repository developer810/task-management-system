<?php



namespace App\Http\Controllers;



use App\Http\Controllers\Controller;

use App\Models\Project;
use App\Models\ProjectMember;
use App\Models\Task;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Yajra\Datatables\DataTables;



class TaskController extends Controller
{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Datatables $datatables, Request $request)
    {

        if ($datatables->getRequest()->ajax()) {

            $items = Task::orderBy('id', 'desc');

            if ($request->start_date && $request->end_date) {
                $start_date = Carbon::parse($request->start_date)->startOfDay();
                $end_date = Carbon::parse($request->end_date)->endOfDay();
                $items = $items->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            }

            global $index;

            $index = 1;

            return $datatables->of($items)

                ->addColumn('status', function ($items) {
                    if ($items->status == 'pending') {
                        $status = '<span class="badge badge-pill badge-danger">Pending</span>';
                    } elseif ($items->status == 'in_process') {
                        $status = '<span class="badge badge-pill badge-info">In Process</span>';
                    } else {
                        $status = '<span class="badge badge-pill badge-success">Completed</span>';
                    }
                    return $status;
                })

                ->addColumn('member_name', function ($items) {
                    if (isset($items->user_id)) {
                        $members = json_decode($items->user_id, true);
                        $arr = [];
                        foreach ($members as $member) {
                            $user = User::where('id',  $member)->first();
                            array_push($arr, $user->name);
                        }
                        return $arr;
                    } else {
                        return '--';
                    }
                })

                ->addColumn('project_name', function ($items) {
                    if (isset($items->project_id)) {
                        $project = Project::where('id', $items->project_id)->first();
                        return $project->name;
                    } else {
                        return '--';
                    }
                })

                ->addColumn('start_date_time', function ($items) {
                    if ($items->start_date_time) {
                        return $items->start_date_time;
                    } else {
                        return '--';
                    }
                })

                ->addColumn('end_date_time', function ($items) {
                    if ($items->end_date_time) {
                        return $items->end_date_time;
                    } else {
                        return '--';
                    }
                })

                ->addColumn('submit_datetime', function ($items) {
                    if ($items->submit_datetime) {
                        return $items->submit_datetime;
                    } else {
                        return '--';
                    }
                })


                ->addColumn('action', function ($items) {
                    $action = '<div class="btn-group btn-group-sm" role="group" aria-label="btnGroup1">';
                    if ($items->status == 'pending') {
                        $action .= '<button title="Active" type="button" class="btn btn-success btn-sm" onclick="status_change(\'' . $items->id . '\')">Start</button>';
                        $action .= '<button title="Edit" type="button" class="btn btn-primary btn-sm" onclick="edit(\'' . $items->id . '\')">Edit</button>';
                        $action .= '<button title="Delete" type="button" class="btn btn-danger btn-sm" onclick="del(\'' . $items->id . '\')">Delete</button></div>';
                    } elseif ($items->status == 'in_process') {
                        $action .= '<button title="Active" type="button" class="btn btn-warning btn-sm" onclick="complete(\'' . $items->id . '\')">Complete</button>';
                    }
                    return $action;
                })

                ->addColumn('id', function ($items) {
                    global $index;
                    $id = $index;
                    $index++;
                    return $id;
                })
                ->rawColumns(['id', 'name', 'member_name', 'project_name', 'description', 'start_date_time', 'end_date_time', 'submit_datetime', 'status', 'action'])
                ->make();
        }
        $members = User::where('role_id', '3')->get();
        $projects = Project::where('status', '1')->get();
        return view('tasks.list', compact('projects', 'members'));
    }

    public function fetchMember(Request $request)
    {
        $data['members'] = ProjectMember::where("project_id", $request->project_id)->with('user')->get(["member_id"]);

        return response()->json($data);
    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()
    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'start_date_time' => 'required',
            'members' => 'nullable',
            'project' => 'nullable',
        ]);
        $task = new Task();
        if ($request->members) {
            $task->user_id = json_encode($request->members);
        } else {
            $task->user_id = NULL;
        }
        $task->project_id = $request->project;
        $task->name = $request->name;
        $task->description = $request->description;
        $task->start_date_time = $request->start_date_time;
        $task->end_date_time = $request->end_date_time;
        $task->status = 'pending';
        $task->created_at = date('Y-m-d H:i:s');

        if ($task->save()) {
            return response()->json(['type' => 'success', 'text' => 'Task added Successfully'], 200);
        }
        return response()->json(['success' => false], 500);
    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show(Task $task)
    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit(Request $request)
    {
        $task = Task::where('id', $request->id)->first();
        $task['members_arr'] = json_decode($task['user_id'], true);
        return $task;
    }

    public function complete(Request $request)
    {
        return Task::find($request->id);
    }

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'start_date_time' => 'required',
            'members' => 'nullable',
            'project' => 'nullable',
        ]);

        $task = Task::find($request->id);
        if ($request->members) {
            $task->user_id = json_encode($request->members);
        } else {
            $task->user_id = NULL;
        }
        $task->project_id = $request->project;
        $task->name = $request->name;
        $task->description = $request->description;
        $task->start_date_time = $request->start_date_time;
        $task->end_date_time = $request->end_date_time;
        $task->updated_at = date('Y-m-d H:i:s');
        if ($task->save()) {
            return response()->json(['type' => 'success', 'text' => 'Task Updated Successfully'], 200);
        }
        return response()->json(['success' => false], 500);
    }


    public function complete_submit(Request $request)
    {
        $task = Task::find($request->id);
        $task->delay_reason = $request->delay_reason;
        $task->status = 'submitted';
        $task->submit_datetime = date('Y-m-d H:i:s');
        $task->updated_at = date('Y-m-d H:i:s');
        if ($task->save()) {
            return response()->json(['type' => 'success', 'text' => 'Task Completed Successfully'], 200);
        }
        return response()->json(['success' => false], 500);
    }

    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy(Request $request)
    {
        $task = Task::find($request->id);
        if ($task->delete()) {
            return response()->json(['type' => 'success', 'text' => 'Task Deleted Successfully'], 200);
        }
        return response()->json(['success' => false], 500);
    }

    function status_change(Request $request)
    {
        if ($request->id) {
            $task = Task::find($request->id);
            if ($task->status == 'pending') {
                $task::where('id', $request->id)
                    ->update([
                        'status' => 'in_process',
                    ]);
                return response()->json(['type' => 'success', 'text' => 'Task Status Changed To In Process'], 200);
            } elseif ($task->status == 'in_process') {
                $task::where('id', $request->id)
                    ->update([
                        'status' => 'submitted',
                    ]);
                return response()->json(['type' => 'success', 'text' => 'Task Status Changed To Completed'], 200);
            }
        }
        return response()->json(['success' => false], 500);
    }
}
