<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function login_submit(Request $request)
    {

        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        if (Auth::check()) {
            return redirect('dashboard');
        }
        $user = User::where('email', $request->email)->whereIn('role_id', [1, 2, 3])->first();
        if ($user) {
            $credentials = [
                'email' => $request->email,
                'password' => $request->password,
            ];
            $remember = $request->remember == 'on' ? true : false;
            if (Auth::attempt($credentials, $remember)) {
                $user = auth()->user();
                return ['type' => 'success', 'text' => 'Login Successfull'];
            }
            return ['type' => 'error', 'text' => 'Invalid email or password '];
        }
        return ['type' => 'error', 'text' => 'Invalid Username'];
    }
}
