<?php



namespace App\Http\Controllers;



use App\Http\Controllers\Controller;

use App\Models\Project;
use App\Models\ProjectMember;
use App\Models\User;
use Illuminate\Http\Request;

use Yajra\Datatables\DataTables;



class ProjectController extends Controller
{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Datatables $datatables)
    {

        if ($datatables->getRequest()->ajax()) {

            $items = Project::orderBy('id', 'desc');

            global $index;

            $index = 1;

            return $datatables->of($items)

                ->addColumn('status', function ($items) {
                    if ($items->status == 1) {
                        $status = '<span class="badge badge-pill badge-success">Active</span>';
                    } else {
                        $status = '<span class="badge badge-pill badge-danger">Inactive</span>';
                    }
                    return $status;
                })

                ->addColumn('client', function ($items) {
                    if ($items->client_id) {
                        $client = User::where('id', $items->client_id)->first();
                        return $client->name;
                    } else {
                        return '--';
                    }
                })

                ->addColumn('start_date', function ($items) {
                    if ($items->start_date) {
                        return $items->start_date;
                    } else {
                        return '--';
                    }
                })

                ->addColumn('end_date', function ($items) {
                    if ($items->end_date) {
                        return $items->end_date;
                    } else {
                        return '--';
                    }
                })

                ->addColumn('members', function ($items) {
                    if ($items->id) {
                        $project_members = ProjectMember::where('project_id', $items->id)->get();
                        if ($project_members) {
                            $member = [];
                            foreach ($project_members as $project_member) {
                                $member_name = User::where('id', $project_member->member_id)->first();
                                if($member_name){
                                    array_push($member, $member_name->name);
                                }
                            }
                            return $member;
                        } else {
                            return '--';
                        }
                    }
                })

                ->addColumn('action', function ($items) {
                    $action = '<div class="btn-group btn-group-sm" role="group" aria-label="btnGroup1">';
                    if ($items->status == 0) {
                        $action .= '<button title="Active" type="button" class="btn btn-success btn-sm" onclick="status_change(\'' . $items->id . '\')">Active</button>';
                    } else {
                        $action .= '<button title="Active" type="button" class="btn btn-warning btn-sm" onclick="status_change(\'' . $items->id . '\')">Inactive</button>';
                    }
                    $action .= '<button title="Edit" type="button" class="btn btn-primary btn-sm" onclick="edit(\'' . $items->id . '\')">Edit</button>';
                    $action .= '<button title="Delete" type="button" class="btn btn-danger btn-sm" onclick="del(\'' . $items->id . '\')">Delete</button></div>';
                    return $action;
                })
                ->addColumn('id', function ($items) {
                    global $index;
                    $id = $index;
                    $index++;
                    return $id;
                })
                ->rawColumns(['id', 'name', 'description', 'start_date', 'end_date', 'client', 'members', 'status', 'action'])
                ->make();
        }
        $clients = User::where('role_id', '4')->get();
        $members = User::whereNot('role_id', '4')->get();
        return view('projects.list', compact('clients', 'members'));
    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()
    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:projects,name',
            'slug' => 'required|max:255|unique:projects,slug',
            'description' => 'required|max:255',
        ]);
        $project = new Project();
        $project->client_id = $request->client;
        $project->name = $request->name;
        $project->slug = $request->slug;
        $project->description = $request->description;
        $project->start_date = $request->start_date;
        $project->end_date = $request->end_date;
        $project->status = '1';
        $project->created_at = date('Y-m-d H:i:s');

        if ($project->save()) {
            if ($request->members) {
                foreach ($request->members as $member) {
                    $project_member = new ProjectMember();
                    $project_member->project_id = $project->id;
                    $project_member->member_id = $member;
                    $project_member->created_at = date('Y-m-d H:i:s');
                    $project_member->save();
                }
            }
            return response()->json(['type' => 'success', 'text' => 'Project added Successfully'], 200);
        }
        return response()->json(['success' => false], 500);
    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show(Project $project)
    {
        //
    }

    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit(Request $request)
    {
        $project = Project::find($request->id);
        $project['members_arr'] = $project->members->pluck('member_id');
        return $project;
    }

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request)
    {
        $project = Project::where('id', $request->id)->first();

        $request->validate([
            'name' => 'required|max:255',
            'slug' => 'required|max:255',
            'description' => 'required|max:255',
        ]);
        $project->client_id = $request->client;
        $project->name = $request->name;
        $project->slug = $request->slug;
        $project->description = $request->description;
        $project->start_date = $request->start_date;
        $project->end_date = $request->end_date;
        $project->updated_at = date('Y-m-d H:i:s');

        if ($request->members) {
            $member_ids = ProjectMember::where('project_id', $request->id)->get()->pluck('member_id')->toArray();
            foreach ($request->members as $member) {
                if (!in_array($member, $member_ids)) {
                    $project_new_member = new ProjectMember();
                    $project_new_member->project_id = $request->id;
                    $project_new_member->member_id = $member;
                    $project_new_member->created_at = date('Y-m-d H:i:s');
                    $project_new_member->save();
                }
            }
            foreach ($member_ids as $member_id) {
                if (!in_array($member_id, $request->members)) {
                    ProjectMember::where('project_id', $request->id)->where('member_id', $member_id)->delete();
                }
            }
        }
        if ($project->save()) {
            return response()->json(['type' => 'success', 'text' => 'Project Updated Successfully'], 200);
        }
        return response()->json(['success' => false], 500);
    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy(Request $request)
    {
        $project = Project::find($request->id);
        if ($project->delete()) {
            return response()->json(['type' => 'success', 'text' => 'Project Deleted Successfully'], 200);
        }
        return response()->json(['success' => false], 500);
    }

    function status_change(Request $request)
    {

        if ($request->id) {

            $project = Project::find($request->id);

            if ($project->status == 0) {

                $project::where('id', $request->id)

                    ->update([

                        'status' => 1,

                    ]);

                return response()->json(['type' => 'success', 'text' => 'Project Activated Successfully'], 200);
            } else {

                $project::where('id', $request->id)

                    ->update([

                        'status' => 0,

                    ]);

                return response()->json(['type' => 'success', 'text' => 'Project Deactivated Successfully'], 200);
            }
        }

        return response()->json(['success' => false], 500);
    }
}
