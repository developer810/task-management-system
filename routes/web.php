<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\PanelController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\SupervisorController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\ClientController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [LoginController::class, 'index'])->name('login')->middleware('web');
Route::post('login/submit', [LoginController::class, 'login_submit'])->name('login.submit')->middleware('guest:admin');

Route::group(['middleware' => 'auth:admin'], function () {

    Route::get('logout', function () {
        Auth::logout();
        return redirect('/');
    })->name('logout');


    Route::get('dashboard', [PanelController::class, 'index'])->name('dashboard');
    Route::get('project', [ProjectController::class, 'index'])->name('project.index');
    Route::post('project/store', [ProjectController::class, 'store'])->name('project.store');
    Route::post('project/status_change', [ProjectController::class, 'status_change'])->name('project.status_change');
    Route::delete('project/delete/{id}', [ProjectController::class, 'destroy'])->name('project.delete');
    Route::get('project/edit/{id}', [ProjectController::class, 'edit'])->name('project.edit');
    Route::post('project/update/{id}', [ProjectController::class, 'update'])->name('project.update');

    Route::get('task', [TaskController::class, 'index'])->name('task.index');
    Route::post('task/store', [TaskController::class, 'store'])->name('task.store');
    Route::post('task/status_change', [TaskController::class, 'status_change'])->name('task.status_change');
    Route::delete('task/delete/{id}', [TaskController::class, 'destroy'])->name('task.delete');
    Route::get('task/edit/{id}', [TaskController::class, 'edit'])->name('task.edit');
    Route::post('task/update/{id}', [TaskController::class, 'update'])->name('task.update');

    Route::get('task/complete/{id}', [TaskController::class, 'complete'])->name('task.complete');
    Route::post('task/complete_submit/{id}', [TaskController::class, 'complete_submit'])->name('task.complete_submit');

    Route::post('fetch-members', [TaskController::class, 'fetchMember']);

    Route::resource('supervisor', SupervisorController::class);
    Route::post('supervisor/status_change', [SupervisorController::class, 'status_change'])->name('supervisor.status_change');

    Route::resource('staff', StaffController::class);
    Route::post('staff/status_change', [StaffController::class, 'status_change'])->name('staff.status_change');

    Route::resource('client', ClientController::class);
    Route::post('client/status_change', [ClientController::class, 'status_change'])->name('client.status_change');

    // Route::resources([
    //     'project' => 'ProjectController',
    // ]);
});

